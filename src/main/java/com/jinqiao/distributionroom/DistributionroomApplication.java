package com.jinqiao.distributionroom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DistributionroomApplication {

	public static void main(String[] args) {
		SpringApplication.run(DistributionroomApplication.class, args);
	}

}
